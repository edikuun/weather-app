import React from 'react';
import { StyleSheet, Text, View, Animated, TextInput, Button, ActivityIndicator } from 'react-native';

// import { DangerZone } from 'expo';
// const { Lottie } = DangerZone;

import { API_KEY } from './utils/WeatherAPIKey';

import Weather from './components/Weather';

import { weatherConditions } from './utils/WeatherConditions';

export default class App extends React.Component {
  state = {
    isLoading: true,
    city: null,
    temperature: 0,
    weatherCondition: null,
    desc: null,
    error: null,
    timeOut: 0,
    typing: false
  };

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.fetchWeather(position.coords.latitude, position.coords.longitude);
      },
      error => {
        this.setState({
          error: 'Error Getting Weather Condtions'
        });
      }
    );
  }

  fetchWeather(lat, lon) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    )
      .then(res => res.json())
      .then(json => {
        console.log(json);
        this.setState({
          city: json.name,
          temperature: json.main.temp,
          weatherCondition: json.weather[0].main,
          desc: json.weather[0].description,
          isLoading: false
        });
      });
  }

  changeCity = (text) => {
    const self = this;

    if (self.state.timeOut) {
      clearTimeout(self.state.timeOut);
    }

    console.log(this.state.isDoneFetching);

    self.setState({
      city: text,
      typing: false,
      timeOut: setTimeout(function () {
        self.fetchLocation(self.state.city);
      }, 3000)
    });
  }

  fetchLocation(city) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${API_KEY}&units=metric`
    )
      .then(res => res.json())
      .then(json => {
        console.log(json);
        this.setState({
          city: json.name,
          temperature: json.main.temp,
          weatherCondition: json.weather[0].main,
          desc: json.weather[0].description,
          isLoading: false
        });
      });
  }

  render() {
    const { isLoading, weatherCondition, temperature, city, desc } = this.state;
    return (
      <View style={styles.container}>
        {isLoading ? (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color="black" />
            <Text style={styles.loadingText}>Fetching The Weather</Text>
          </View>
        ) : (
            <View style={{ flex: 1 }}>
              <View style={[styles.textContainer,
              { backgroundColor: weatherConditions[weatherCondition].color }]}>
                <TextInput
                  style={{ height: 40, fontSize: 30, color: 'white', textAlign: 'center' }}
                  placeholder="Enter your location"
                  onChangeText={(text) => this.changeCity(text)}
                />
              </View>
              <Weather weather={weatherCondition} temperature={temperature} city={city} desc={desc} />
            </View>
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  textContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  loadingText: {
    fontSize: 30
  }
});