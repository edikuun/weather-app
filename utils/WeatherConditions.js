export const weatherConditions = {
    Rain: {
        color: '#0D8A9C',
        title: "It's Raining",
        subtitle: 'Bring an umbrella with you',
        icon: 'weather-rainy'
    },
    Clear: {
        color: '#f7b733',
        title: "It's Sunny",
        subtitle: "Don't forget to wear sunglasses",
        icon: 'weather-sunny'
    },
    Thunderstorm: {
        color: '#616161',
        title: 'A Storm is coming',
        subtitle: 'Please stay inside your houses',
        icon: 'weather-lightning'
    },
    Clouds: {
        color: '#755985',
        title: "It's cloudy today",
        subtitle: 'It might rain later',
        icon: 'weather-cloudy'
    },

    Snow: {
        color: '#00d2ff',
        title: 'Snow',
        subtitle: 'Get out and build a snowman for me',
        icon: 'weather-snowy'
    },
    Drizzle: {
        color: '#076585',
        title: 'Drizzle',
        subtitle: 'Partially raining...',
        icon: 'weather-hail'
    },
    Haze: {
        color: '#66A6FF',
        title: 'Haze',
        subtitle: 'Another name for Partial Raining',
        icon: 'weather-hail'
    },
    Mist: {
        color: '#3CD3AD',
        title: 'Mist',
        subtitle: "Don't roam in forests!",
        icon: 'weather-fog'
    }
};